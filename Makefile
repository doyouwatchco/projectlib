# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pguitar <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/09 13:47:22 by pguitar           #+#    #+#              #
#    Updated: 2019/04/09 17:31:26 by pguitar          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

CFLAGS = -c -Wall -Wextra -Werror

SRCS = ft_atoi.c \
	   ft_bzero.c \
	   ft_isalnum.c \
	   ft_isalpha.c \
	   ft_isascii.c \
	   ft_isdigit.c \
	   ft_isprint.c \
	   ft_memccpy.c \
	   ft_memchr.c \
	   ft_memcmp.c \
	   ft_memcpy.c \
	   ft_memmove.c \
	   ft_memset.c \
	   ft_strcat.c \
	   ft_strchr.c \
	   ft_strcmp.c \
	   ft_strcpy.c \
	   ft_strdup.c \
	   ft_strlcat.c \
	   ft_strlen.c \
	   ft_strncat.c \
	   ft_strncmp.c \
	   ft_strncpy.c \
	   ft_strnstr.c \
	   ft_strrchr.c \
	   ft_strstr.c \
	   ft_tolower.c \
	   ft_toupper.c \
	   ft_putchar.c \
	   ft_putstr.c \
	   ft_putendl.c \
	   ft_putnbr.c \
	   ft_itoa.c \
	   ft_strsplit.c \
	   ft_strtrim.c \
	   ft_strjoin.c \
	   ft_strsub.c \
	   ft_strnequ.c \
	   ft_strequ.c \
	   ft_strmapi.c \
	   ft_strmap.c \
	   ft_striteri.c \
	   ft_striter.c \
	   ft_strclr.c \
	   ft_strdel.c \
	   ft_strnew.c \
	   ft_memdel.c \
	   ft_memalloc.c \
	   ft_putchar_fd.c \
	   ft_putstr_fd.c \
	   ft_putendl_fd.c \
	   ft_putnbr_fd.c \
	   ft_lstnew.c \
	   ft_lstdelone.c \
	   ft_lstdel.c \
	   ft_lstadd.c \
	   ft_lstiter.c \
	   ft_lstmap.c \
	   ft_min.c \
	   ft_max.c \
	   ft_isupper.c \
	   ft_islower.c \
	   ft_strnlen.c

OSRCS = *.o

INCLUDES = -I libft.h

all : $(NAME)

$(NAME):
	gcc $(CFLAGS) $(INCLUDES) $(SRCS) \
		&& ar rc $(NAME) $(OSRCS) \
		&& ranlib $(NAME)

clean:
	rm -f *.o

fclean: clean
	rm -f $(NAME)

re: fclean all

